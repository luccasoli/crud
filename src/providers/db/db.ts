import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

const STORAGE_KEY = 'crud';

@Injectable()
export class DbProvider {

  constructor(public storage: Storage) {
    console.log('Hello DbProvider Provider');
  }

  updateStorage(listTexts: any[]) {
  	this.getAllListTexts()
  		.then(sucess => {
  			this.storage.set(STORAGE_KEY, JSON.stringify(listTexts));
  		})
  		.catch(error => {
  			console.log('Erro no insert do dbProvider: ' + error);
  		});
  }

  getAllListTexts() {
  	return this.storage.get(STORAGE_KEY);
  }

}

export class Item {
  header: string;
  text: string;

  constructor(header?, text?) {
    this.header = header;
    this.text = text;
  }
}
