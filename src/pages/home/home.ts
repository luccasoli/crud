import { Component } from '@angular/core';
import { NavController, ModalController} from 'ionic-angular';
import { Item, DbProvider } from '../../providers/db/db';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  listTexts: any[] = [];

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, private db: DbProvider, private alertCtrl: AlertController) {

  }

  ionViewDidLoad() {
  	// this.db.updateStorage(null);
    this.db.getAllListTexts()
    	.then(data => {
    		if(data) {
    			console.log(data);
    			this.listTexts = JSON.parse(data);
    		}
    		else {
    			console.log(data);
    			this.listTexts = [];
    		}
    	})
    	.catch(e => {
    		console.log("Erro na inicialização da lista: " + e);
    	});
  }

  showEditModal(item) {
    let editModal = this.modalCtrl.create('EditDataPage', {'item': item});
    editModal.present();
    editModal.onDidDismiss(data => {
      if(data) {
        for(let i = 0; i < this.listTexts.length; i++){
          if(this.listTexts[i] == item) {
            console.log("Item modificado");
            this.listTexts[i] = data;
          }
        }
      }
    });
  }

  remove(item) {
    let alert = this.alertCtrl.create({
      title: 'Exclusão',
      message: 'Tem certeza que deseja remover?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Excluir',
          handler: () => {
            for(let i = 0; i < this.listTexts.length; i++) {
              if(this.listTexts[i] == item){
                this.listTexts.splice(i, 1);
                console.log("Item removido com sucesso");
                this.db.updateStorage(this.listTexts);
                break;
              }
            }
          }
        }
      ]
    });
    alert.present();    
  }

  showInsertModal() {
  	let insertModal = this.modalCtrl.create('InsertDataPage');
    insertModal.present();

    insertModal.onDidDismiss(data => {  
    	if(data != null) {
    		console.log(data.item);
	      this.listTexts.push(data.item);
	      this.db.updateStorage(this.listTexts);
    	}
    	else {
    		console.log("Nada será inserido");
    	}
    });
  }
}
