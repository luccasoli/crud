import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InsertDataPage } from './insert-data';

@NgModule({
  declarations: [
    InsertDataPage,
  ],
  imports: [
    IonicPageModule.forChild(InsertDataPage),
  ],
})
export class InsertDataPageModule {}
