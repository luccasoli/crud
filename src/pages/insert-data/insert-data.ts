import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Item } from '../../providers/db/db';

/**
 * Generated class for the InsertDataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-insert-data',
  templateUrl: 'insert-data.html',
})
export class InsertDataPage {

  header: string = "";
  text: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InsertDataPage');
  }

  save() {
    if(this.header && this.text){
      console.log("tem");
      let item = new Item(this.header, this.text);
      this.viewCtrl.dismiss({item: item});
    }
    else {
      console.log("nao tem");

      this.viewCtrl.dismiss(null);
    }
  }

}
