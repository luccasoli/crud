import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Item, DbProvider } from '../../providers/db/db';

/**
 * Generated class for the EditDataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-data',
  templateUrl: 'edit-data.html',
})
export class EditDataPage {

	item: Item;

  constructor(public navCtrl: NavController, public navParams: NavParams, private db: DbProvider, public viewCtrl: ViewController) {
  	this.item = this.navParams.get("item");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditDataPage');
  }

  modify() {
  	this.viewCtrl.dismiss(this.item);
  }

  cancel() {
  	this.viewCtrl.dismiss(null);
  }

}
